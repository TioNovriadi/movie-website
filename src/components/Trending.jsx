import React from "react";
import { Card, Col, Container, Image, Row } from "react-bootstrap";
import spidermanPoster from "../assets/poster/spiderman.jpg";
import shazamPoster from "../assets/poster/shazam.jpg";
import gotgPoster from "../assets/poster/gotg.jpeg";
import superMarioPoster from "../assets/poster/superMario.jpg";
import wakandaPoster from "../assets/poster/wakanda.jpeg";
import legoPoster from "../assets/poster/lego.jpg";

const Trending = () => {
  return (
    <div>
      <Container>
        <br />
        <br />
        <h1 className="text-white">TRENDING MOVIES</h1>
        <Row>
          <Col md={4} id="trending">
            <Card className="bg-dark text-white moviePoster">
              <Image
                src={spidermanPoster}
                alt="Spiderman Poster"
                width="100%"
                height="auto"
                className="posters"
              />

              <div className="p-2 m-1">
                <Card.Title className="text-center">
                  Spider-man: Into the Spider-Verse 2
                </Card.Title>

                <Card.Text className="text-left">
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content.
                </Card.Text>
              </div>
            </Card>
          </Col>

          <Col md={4}>
            <Card className="bg-dark text-white moviePoster">
              <Image
                src={shazamPoster}
                alt="Shazam Poster"
                width="100%"
                height="auto"
                className="posters"
              />

              <div className="p-2 m-1">
                <Card.Title className="text-center">
                  Shazam: Fury of the God
                </Card.Title>

                <Card.Text className="text-left">
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content.
                </Card.Text>
              </div>
            </Card>
          </Col>

          <Col md={4}>
            <Card className="bg-dark text-white moviePoster">
              <Image
                src={gotgPoster}
                alt="GOTG Poster"
                width="100%"
                height="auto"
                className="posters"
              />

              <div className="p-2 m-1">
                <Card.Title className="text-center">
                  Guardian of the Galaxy Vol. 3
                </Card.Title>

                <Card.Text className="text-left">
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content.
                </Card.Text>
              </div>
            </Card>
          </Col>

          <Col md={4}>
            <Card className="bg-dark text-white moviePoster">
              <Image
                src={superMarioPoster}
                alt="Super Mario Poster"
                width="100%"
                height="auto"
                className="posters"
              />

              <div className="p-2 m-1">
                <Card.Title className="text-center">
                  The Super Mario Bros
                </Card.Title>

                <Card.Text className="text-left">
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content.
                </Card.Text>
              </div>
            </Card>
          </Col>

          <Col md={4}>
            <Card className="bg-dark text-white moviePoster">
              <Image
                src={wakandaPoster}
                alt="Wakanda Poster"
                width="100%"
                height="auto"
                className="posters"
              />

              <div className="p-2 m-1">
                <Card.Title className="text-center">Wakanda Forever</Card.Title>

                <Card.Text className="text-left">
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content.
                </Card.Text>
              </div>
            </Card>
          </Col>

          <Col md={4}>
            <Card className="bg-dark text-white moviePoster">
              <Image
                src={legoPoster}
                alt="Lego Poster"
                width="100%"
                height="auto"
                className="posters"
              />

              <div className="p-2 m-1">
                <Card.Title className="text-center">
                  The Lego Movie 2
                </Card.Title>

                <Card.Text className="text-left">
                  This is a wider card with supporting text below as a natural
                  lead-in to additional content.
                </Card.Text>
              </div>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Trending;
